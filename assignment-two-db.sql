CREATE DATABASE IF NOT EXISTS `assignment-two-db` ;

USE 'assignment-two-db';

CREATE TABLE `assignment-two-db`.`user` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `username` VARCHAR(45) NOT NULL,
  `password` VARCHAR(45) NOT NULL,
  `type` VARCHAR(45) NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `username_UNIQUE` (`username` ASC));

CREATE TABLE `assignment-two-db`.`flight` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `number` INT NOT NULL,
  `plane_type` VARCHAR(45) NOT NULL,
  `depart_city` INT NOT NULL,
  `depart_time` DATETIME NOT NULL,
  `arriv_city` INT NOT NULL,
  `arriv_time` DATETIME NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_flight_1_idx` (`depart_city` ASC),
  CONSTRAINT `fk_flight_1`
  FOREIGN KEY (`depart_city`)
  REFERENCES `assignment-two-db`.`city` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  INDEX `fk_flight_2_idx` (`arriv_city` ASC),
  CONSTRAINT `fk_flight_2`
  FOREIGN KEY (`arriv_city`)
  REFERENCES `assignment-two-db`.`city` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);

CREATE TABLE `assignment-two-db`.`city` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `lat` DOUBLE NOT NULL,
  `lon` DOUBLE NOT NULL,
  PRIMARY KEY (`id`));

CREATE TABLE `assignment-two-db`.`ticket` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `id_user` INT NOT NULL,
  `id_flight` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_ticket_1_idx` (`id_user` ASC),
  INDEX `fk_ticket_2_idx` (`id_flight` ASC),
  CONSTRAINT `fk_ticket_1`
  FOREIGN KEY (`id_user`)
  REFERENCES `assignment-two-db`.`user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_ticket_2`
  FOREIGN KEY (`id_flight`)
  REFERENCES `assignment-two-db`.`flight` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);

INSERT INTO `assignment-two-db`.`city` (`id`, `name`, `lat`, `lon`) VALUES ('1', 'Barcelona', '41.39020538330078', '2.15401');
INSERT INTO `assignment-two-db`.`city` (`id`, `name`, `lat`, `lon`) VALUES ('2', 'Berlin', '52.52000045776367', '13.405');
INSERT INTO `assignment-two-db`.`city` (`id`, `name`, `lat`, `lon`) VALUES ('3', 'New York', '40.7128', '74.0060');
INSERT INTO `assignment-two-db`.`city` (`id`, `name`, `lat`, `lon`) VALUES ('4', 'Liverpool', '53.4084', '2.9916');
INSERT INTO `assignment-two-db`.`city` (`id`, `name`, `lat`, `lon`) VALUES ('5', 'Madrid', '40.4168', '3.7038');
INSERT INTO `assignment-two-db`.`city` (`id`, `name`, `lat`, `lon`) VALUES ('6', 'Cluj-Napoca', '46.7712', '23.6236');
INSERT INTO `assignment-two-db`.`city` (`id`, `name`, `lat`, `lon`) VALUES ('7', 'Paris', '48.8566', '2.3522');
INSERT INTO `assignment-two-db`.`city` (`id`, `name`, `lat`, `lon`) VALUES ('8', 'Moskau', '55.7558', '37.6173');
INSERT INTO `assignment-two-db`.`city` (`id`, `name`, `lat`, `lon`) VALUES ('9', 'Botswana', '22.3285', '24.6849');
INSERT INTO `assignment-two-db`.`city` (`id`, `name`, `lat`, `lon`) VALUES ('10', 'Tokyo', '35.6895', '139.6917');
INSERT INTO `assignment-two-db`.`city` (`id`, `name`, `lat`, `lon`) VALUES ('11', 'Hong Kong', '22.3964', '114.1095');
INSERT INTO `assignment-two-db`.`city` (`id`, `name`, `lat`, `lon`) VALUES ('12', 'London', '51.5074', '0.1278');