package entities;

import javax.persistence.*;
import java.util.Collection;

/**
 * Created by constantin on 10/26/17.
 */
@Entity
public class City {
    private int id;
    private String name;
    private double lat;
    private double lon;

    @Id
    @Column(name = "id", nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "name", nullable = false, length = 45)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "lat", nullable = false)
    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    @Basic
    @Column(name = "lon", nullable = false)
    public double getLon() {
        return lon;
    }

    public void setLon(double lon) {
        this.lon = lon;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        City city = (City) o;

        if (id != city.id) return false;
        if (lat != city.lat) return false;
        if (lon != city.lon) return false;
        if (name != null ? !name.equals(city.name) : city.name != null) return false;

        return true;
    }
}
