package entities;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Collection;

/**
 * Created by constantin on 10/26/17.
 */
public class Flight {
    private int id;
    private int number;
    private String planeType;
    private int departCity;
    private Timestamp departTime;
    private int arrivCity;
    private Timestamp arrivTime;

    public Flight() {}

    public Flight(int number, String planeType, int departCity, Timestamp departTime, int arrivCity, Timestamp arrivTime) {
        this.number = number;
        this.planeType = planeType;
        this.departCity = departCity;
        this.departTime = departTime;
        this.arrivCity = arrivCity;
        this.arrivTime = arrivTime;
    }

    public Flight(Integer id, Integer number, String plane_type, int depart_city, Timestamp depart_time, int arriv_city, Timestamp arriv_time) {
        this.id = id;
        this.number = number;
        this.planeType = plane_type;
        this.departCity = depart_city;
        this.departTime = depart_time;
        this.arrivCity = arriv_city;
        this.arrivTime = arriv_time;

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

      public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public String getPlaneType() {
        return planeType;
    }

    public void setPlaneType(String planeType) {
        this.planeType = planeType;
    }

    public int getDepartCity() {
        return departCity;
    }

    public void setDepartCity(int departCity) {
        this.departCity = departCity;
    }

    public Timestamp getDepartTime() {
        return departTime;
    }

    public void setDepartTime(Timestamp departTime) {
        this.departTime = departTime;
    }

    public int getArrivCity() {
        return arrivCity;
    }

    public void setArrivCity(int arrivCity) {
        this.arrivCity = arrivCity;
    }

    public Timestamp getArrivTime() {
        return arrivTime;
    }

    public void setArrivTime(Timestamp arrivTime) {
        this.arrivTime = arrivTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Flight flight = (Flight) o;

        if (id != flight.id) return false;
        if (number != flight.number) return false;
        if (departCity != flight.departCity) return false;
        if (arrivCity != flight.arrivCity) return false;
        if (planeType != null ? !planeType.equals(flight.planeType) : flight.planeType != null) return false;
        if (departTime != null ? !departTime.equals(flight.departTime) : flight.departTime != null) return false;
        if (arrivTime != null ? !arrivTime.equals(flight.arrivTime) : flight.arrivTime != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + number;
        result = 31 * result + (planeType != null ? planeType.hashCode() : 0);
        result = 31 * result + departCity;
        result = 31 * result + (departTime != null ? departTime.hashCode() : 0);
        result = 31 * result + arrivCity;
        result = 31 * result + (arrivTime != null ? arrivTime.hashCode() : 0);
        return result;
    }

}
