package servlets;

import dao.UserDAO;
import entities.User;
import org.hibernate.cfg.Configuration;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by constantin on 10/25/17.
 */
@WebServlet(name = "LoginServlet")
public class LoginServlet extends HttpServlet {

    private UserDAO userDAO;

    public LoginServlet( ) {
        userDAO = new UserDAO(new Configuration().configure().buildSessionFactory());
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        request.getRequestDispatcher("link.html").include(request, response);

        String name = request.getParameter("name");
        String password = request.getParameter("password");

        User u = userDAO.getByUsername(name);

        if (u != null) {

            if (u.getPassword().equals(password)) {

                String type = u.getType();

                out.print("<html><body><h1>Welcome " + name);

                if(type.equals("admin")){
                    out.print("! You have administrator privileges. Go to your Profile page now!");
                } else {
                    out.print("! You can got to the Profile page to view available flights!");
                }
                out.print("</h1></body></html>");
                HttpSession session = request.getSession();
                session.setAttribute("name", name);
                session.setAttribute("type", type);
            } else {
                out.print("Sorry, wrong password");
                request.getRequestDispatcher("login.html").include(request, response);
            }
        } else {
            out.print("Sorry, wrong username!");
            request.getRequestDispatcher("login.html").include(request, response);
        }
        out.close();

    }
}