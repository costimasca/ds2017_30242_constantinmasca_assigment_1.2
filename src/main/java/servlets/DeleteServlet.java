package servlets;

import dao.FlightDAO;
import org.hibernate.cfg.Configuration;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by constantin on 10/30/17.
 */
@WebServlet(name = "DeleteServlet")
public class DeleteServlet extends HttpServlet {
    private FlightDAO flightDAO;

    public DeleteServlet() {
        this.flightDAO = new FlightDAO(new Configuration().configure().buildSessionFactory());
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        int id = Integer.parseInt(request.getParameter("id"));

        response.setContentType("text/html");

        PrintWriter out = response.getWriter();

        HttpSession session = request.getSession();
        String type = (String)session.getAttribute("type");

        if(type != null && type.equals("admin")) {
            try {
                flightDAO.delete(id);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
