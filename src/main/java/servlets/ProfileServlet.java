package servlets;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by constantin on 10/25/17.
 */
public class ProfileServlet extends HttpServlet {
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html");
        PrintWriter out=response.getWriter();
        request.getRequestDispatcher("link.html").include(request, response);

        HttpSession session=request.getSession();
        if(session!=null){
            String name=(String)session.getAttribute("name");

            if(name != null) {

                out.print("<body><h1>Hello, " + name + ", welcome to your profile page!</h1></body>");

                String type = (String) session.getAttribute("type");

                if (type.equals("admin")) {
                    request.getRequestDispatcher("AdminServlet").include(request, response);
                } else if (type.equals("client")) {
                    request.getRequestDispatcher("ClientServlet").include(request, response);
                }
            }else{
                out.print("Please login first");
                request.getRequestDispatcher("login.html").include(request, response);
            }
        }
        else{
            out.print("Please login first");
            request.getRequestDispatcher("login.html").include(request, response);
        }
        out.close();
    }
}
