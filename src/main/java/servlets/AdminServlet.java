package servlets;

import dao.CityDAO;
import dao.FlightDAO;
import entities.Flight;
import org.hibernate.cfg.Configuration;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSetMetaData;
import java.util.List;

/**
 * Created by constantin on 10/26/17.
 */
@WebServlet(name = "AdminServlet")
public class AdminServlet extends HttpServlet {
    private FlightDAO flightDAO;
    private CityDAO cityDAO;

    public AdminServlet() {
        this.flightDAO = new FlightDAO(new Configuration().configure().buildSessionFactory());
        this.cityDAO = new CityDAO(new Configuration().configure().buildSessionFactory());
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        response.setContentType("text/html");

        PrintWriter out = response.getWriter();

        HttpSession session = request.getSession();

        String type = (String)session.getAttribute("type");

        if(type != null && type.equals("admin")) {
            List res = flightDAO.getFlights();
            try {
             dumpFlights(res, out);
             displayMenu(out);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            out.print("Sorry, you do not have permissions for this page!");
            request.getRequestDispatcher("login.html").include(request, response);
        }
    }

    private void displayMenu(java.io.PrintWriter out) {
        out.println("<form action=\"createFlight.html\">\n" +
                "<input type=\"submit\" value=\"New Flight!\">\n" +
                "</form>");

    }

    private int dumpFlights(List rs, java.io.PrintWriter out)
            throws Exception {

        out.println("<P ALIGN='center'><TABLE BORDER=1>");
        out.println("<TR>");
        out.println("<TH>" + "Flight Number" + "</TH>");
        out.println("<TH>" + "Plane Type" + "</TH>");
        out.println("<TH>" + "Departure City" + "</TH>");
        out.println("<TH>" + "Departure Time" + "</TH>");
        out.println("<TH>" + "Arrival City" + "</TH>");
        out.println("<TH>" + "Arrival Time" + "</TH>");
        out.println("</TR>");
        // the data
        for(int i = 0; i < rs.size(); i++) {
            out.println("<TR>");

            Flight f = (Flight) rs.get(i);
            out.println("<TD>" + f.getNumber() + "</TD>");
            out.println("<TD>" + f.getPlaneType() + "</TD>");
            out.println("<TD>" + cityDAO.getById(f.getDepartCity()).getName() + "</TD>");
            out.println("<TD>" + f.getDepartTime() + "</TD>");
            out.println("<TD>" + cityDAO.getById(f.getArrivCity()).getName() + "</TD>");
            out.println("<TD>" + f.getArrivTime() + "</TD>");
            out.println("<TD><a href = \"EditServlet?id=" + f.getId() + "\" > edit </a></TD>");
            out.println("<TD><a href = \"DeleteServlet?id=" + f.getId() + "\" > delete</a></TD>");
            out.println("</TR>");
        }
        out.println("</TABLE></P>");
        return rs.size();
    }
}
