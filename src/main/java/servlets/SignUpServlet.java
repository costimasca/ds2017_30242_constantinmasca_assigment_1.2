package servlets;

import dao.UserDAO;
import entities.User;
import org.hibernate.cfg.Configuration;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by constantin on 10/25/17.
 */
@WebServlet(name = "SignUpServlet")
public class SignUpServlet extends HttpServlet {
    private UserDAO userDAO;

    public SignUpServlet() {
        userDAO = new UserDAO(new Configuration().configure().buildSessionFactory());
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        request.getRequestDispatcher("link.html").include(request, response);

        String name = request.getParameter("name");
        String password = request.getParameter("password");

        /*
        Check if user was created
         */
        User u = userDAO.signUp(name, password);
        out.print("Thank you for signing up, " + u.getUsername());
        HttpSession session = request.getSession();
        session.setAttribute("name", u.getUsername());
        session.setAttribute("type", u.getType());

        request.getRequestDispatcher("login.html").include(request, response);

        out.close();

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
