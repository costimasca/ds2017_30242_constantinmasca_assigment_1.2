package servlets;

import dao.CityDAO;
import dao.FlightDAO;
import entities.Flight;
import org.hibernate.cfg.Configuration;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Date;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;

/**
 * Created by constantin on 10/30/17.
 */
@WebServlet(name = "EditServlet")
public class EditServlet extends HttpServlet {
    private CityDAO cityDAO;
    private FlightDAO flightDAO;

    public EditServlet() {
        this.flightDAO =  new FlightDAO(new Configuration().configure().buildSessionFactory());
        this.cityDAO = new CityDAO(new Configuration().configure().buildSessionFactory());
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        response.setContentType("text/html");

        HttpSession session = request.getSession();
        String type = (String) session.getAttribute("type");

        String docType =
                "<!doctype html public \"-//w3c//dtd html 4.0 " +
                        "transitional//en\">\n";

        if (type != null && type.equals("admin")) try {

            Integer id = Integer.parseInt(request.getParameter("id"));

            Integer number = Integer.parseInt(request.getParameter("number"));
            String plane_type = request.getParameter("plane_type");

            String depart_city_string = request.getParameter("depart_city");
            String arriv_city_string = request.getParameter("arriv_city");
            int depart_city = cityDAO.getIdByName(depart_city_string);
            int arriv_city = cityDAO.getIdByName(arriv_city_string);

            Timestamp depart_time = Timestamp.valueOf(request.getParameter("depart_time").replace("T", " ") + ":00");
            Timestamp arriv_time = Timestamp.valueOf(request.getParameter("arriv_time").replace("T", " ") + ":00");

            flightDAO.update(id,number, plane_type, depart_city, depart_time, arriv_city, arriv_time);

            out.println(docType +
                    "<html>" +
                    "<head><title></title>Flight Updated</title></head>" +
                    "<body>" +
                    "<h1>Flight was successfully updated</h1>" +
                    "<a href=\"AdminServlet\">See Flights</a>" +
                    "</body>" +
                    "</html>");
        } catch (Exception e) {
            e.printStackTrace();
        }
        else {
            out.print("Sorry, you do not have permissions for this page!");
            request.getRequestDispatcher("login.html").include(request, response);
        }

        out.close();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        response.setContentType("text/html");

        PrintWriter out=response.getWriter();

        int id=Integer.parseInt(request.getParameter("id"));

        out.println("<h1>Update Flight</h1>");

        response.setContentType("text/html");

        Flight f = flightDAO.getById(id);


        String tmp = new SimpleDateFormat("yyyy-MM-dd HH:mm").format(f.getArrivTime());
        String arriv_time = tmp.split(" ")[0] + "T" + tmp.split(" ")[1];
        tmp = new SimpleDateFormat("yyyy-MM-dd HH:mm").format(f.getArrivTime());
        String depart_time = tmp.split(" ")[0] + "T" + tmp.split(" ")[1];

        out.print("<form action='EditServlet' method='post'>");
        out.print("<table>");
        out.print("<tr><td></td><td><input type='hidden' name='id' value='"+f.getId()+"'/></td></tr>");
        out.print("<tr><td>Flight Number:</td><td><input type='text' name='number' value='"+f.getNumber()+"'/></td></tr>");
        out.print("<tr><td>Plane Type:</td><td><input type='text' name='plane_type' value='"+f.getPlaneType()+"'/></td></tr>");
        out.print("<tr><td>Departure City:</td><td><input type='text' name='depart_city' value='"+cityDAO.getById(f.getDepartCity()).getName()+"'/></td></tr>");
        out.print("<tr><td>Arrival City:</td><td><input type='text' name='arriv_city' value='"+cityDAO.getById(f.getArrivCity()).getName()+"'/></td></tr>");
        out.print("<tr><td>Departure time:</td><td><input type='datetime-local' name='depart_time' value='" + depart_time + "'/></td></tr>");
        out.print("<tr><td>Arrival time:</td><td><input type='datetime-local' name='arriv_time' value='" + arriv_time + "'/></td></tr>");

        out.print("<tr><td colspan='2'><input type='submit' value='Edit & Save '/></td></tr>");
        out.print("</table>");
        out.print("</form>");

        out.print("<a href=\"ProfileServlet\">Go Back</a>");

        out.close();
    }
}
