package servlets;

import dao.CityDAO;
import dao.FlightDAO;
import entities.Flight;
import org.hibernate.cfg.Configuration;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

/**
 * Created by constantin on 10/31/17.
 */
@WebServlet(name = "ClientServlet")
public class ClientServlet extends HttpServlet {
    private FlightDAO flightDAO;
    private CityDAO cityDAO;

    public ClientServlet() {
        this.flightDAO = new FlightDAO(new Configuration().configure().buildSessionFactory());
        this.cityDAO = new CityDAO(new Configuration().configure().buildSessionFactory());
    }
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");

        PrintWriter out = response.getWriter();

        HttpSession session = request.getSession();

        String type = (String)session.getAttribute("type");

        if(type != null) {
            List res = flightDAO.getFlights();
            try {
                dumpFlights(res, out);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            out.print("Sorry, you have to login first!");
            request.getRequestDispatcher("login.html").include(request, response);
        }
    }

    private int dumpFlights(List rs, java.io.PrintWriter out)
            throws Exception {

        out.println("<P ALIGN='center'><TABLE BORDER=1>");
        out.println("<TR>");
        out.println("<TH>" + "Flight Number" + "</TH>");
        out.println("<TH>" + "Plane Type" + "</TH>");
        out.println("<TH>" + "Departure City" + "</TH>");
        out.println("<TH>" + "Departure Time" + "</TH>");
        out.println("<TH>" + "Arrival City" + "</TH>");
        out.println("<TH>" + "Arrival Time" + "</TH>");
        out.println("</TR>");

        for(int i = 0; i < rs.size(); i++) {
            out.println("<TR>");

            Flight f = (Flight) rs.get(i);
            out.println("<TD>" + f.getNumber() + "</TD>");
            out.println("<TD>" + f.getPlaneType() + "</TD>");
            out.println("<TD>" + cityDAO.getById(f.getDepartCity()).getName() + "</TD>");
            out.println("<TD>" + f.getDepartTime() + "</TD>");
            out.println("<TD>" + cityDAO.getById(f.getArrivCity()).getName() + "</TD>");
            out.println("<TD>" + f.getArrivTime() + "</TD>");
            out.println("<TD><a href = \"TimezoneServlet?doa=arriv&id=" + f.getId() + "\" >Local arrival time</a></TD>");
            out.println("<TD><a href = \"TimezoneServlet?doa=depart&id=" + f.getId() + "\" >Local departure time</a></TD>");
            out.println("</TR>");
        }
        out.println("</TABLE></P>");
        return rs.size();
    }
}
