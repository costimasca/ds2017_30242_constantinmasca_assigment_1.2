package servlets;

import dao.CityDAO;
import dao.FlightDAO;
import entities.City;
import entities.Flight;
import org.hibernate.cfg.Configuration;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by constantin on 10/26/17.
 */
@WebServlet(name = "TimezoneServlet")
public class TimezoneServlet extends HttpServlet {
    private final String USER_AGENT = "Mozilla/5.0";
    private FlightDAO flightDAO;
    private CityDAO cityDAO;

    public TimezoneServlet(){
        this.flightDAO = new FlightDAO(new Configuration().configure().buildSessionFactory());
        this.cityDAO = new CityDAO(new Configuration().configure().buildSessionFactory());
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");

        PrintWriter out = response.getWriter();

        HttpSession session = request.getSession();
        String type = (String) session.getAttribute("type");

        if (type != null) {
            try {
                int id=Integer.parseInt(request.getParameter("id"));
                String doa = request.getParameter("doa");
                Flight f = flightDAO.getById(id);

                City c = null;

                if(doa.equals("arriv")){
                    c = cityDAO.getById(f.getArrivCity());
                } else if(doa.equals("depart")){
                    c = cityDAO.getById(f.getDepartCity());
                }
                String date = getTimezone(c.getLat(),c.getLon());
                out.print("<body><p>Local time for " + c.getName() +
                        " is "+ date + "!</p></body>");
                out.print("<a href=\"ProfileServlet\">Go Back</a>");
            } catch(Exception e) {
                e.printStackTrace();
        }
    } else {
            out.print("Sorry, you have to login first!");
            request.getRequestDispatcher("login.html").include(request, response);
        }
}

    private String getTimezone(double lat, double lon) throws Exception {
        String date = null;
        String url = "http://new.earthtools.org/timezone/";
        url += String.valueOf(lat) + "/";
        url += String.valueOf(lon);

        URL obj = new URL(url);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();

        // optional default is GET
        con.setRequestMethod("GET");

        //add request header
        con.setRequestProperty("User-Agent", USER_AGENT);

             BufferedReader in = new BufferedReader(
                new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();

        String xml = response.toString();
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder;
        InputSource is;

        try {
            builder = factory.newDocumentBuilder();
            is = new InputSource(new StringReader(xml));
            Document doc = builder.parse(is);
            NodeList list = doc.getElementsByTagName("localtime");
            date = list.item(0).getTextContent();
        } catch (ParserConfigurationException e) {
        } catch (SAXException e) {
        } catch (IOException e) {
        }
        return date;
    }

}