package servlets;

import dao.CityDAO;
import dao.FlightDAO;
import org.hibernate.cfg.Configuration;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Time;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Created by constantin on 10/29/17.
 */
@WebServlet(name = "CreateFlightServlet")
public class CreateFlightServlet extends HttpServlet {

    private FlightDAO flightDAO;
    private CityDAO cityDAO;

    public CreateFlightServlet() {
        this.flightDAO = new FlightDAO(new Configuration().configure().buildSessionFactory());
        this.cityDAO = new CityDAO(new Configuration().configure().buildSessionFactory());
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        response.setContentType("text/html");
        request.getRequestDispatcher("link.html").include(request, response);

        HttpSession session = request.getSession();
        String type = (String)session.getAttribute("type");

        String docType =
                "<!doctype html public \"-//w3c//dtd html 4.0 " +
                        "transitional//en\">\n";

        if(type != null && type.equals("admin")) try {

            Integer number = Integer.parseInt(request.getParameter("number"));
            String plane_type =request.getParameter("plane_type");

            String depart_city_string = request.getParameter("depart_city");
            String arriv_city_string = request.getParameter("arriv_city");
            int depart_city = cityDAO.getIdByName(depart_city_string);
            int arriv_city = cityDAO.getIdByName(arriv_city_string);

            Timestamp depart_time = Timestamp.valueOf(request.getParameter("depart_time").replace("T"," ") + ":00");
            Timestamp arriv_time = Timestamp.valueOf(request.getParameter("arriv_time").replace("T"," ") + ":00");

            flightDAO.saveFlight(number,plane_type,depart_city,depart_time,arriv_city,arriv_time);

            out.println(docType +
                    "<html>" +
                    "<head><title></title>Flight Created</title></head>" +
                    "<body>"+
                    "<h1>Flight was successfully created</h1>"+
                    "<a href=\"AdminServlet\">See Flights</a>"+
                    "</body>" +
                    "</html>");
        } catch (Exception e) {
            e.printStackTrace();
        }
        else {
            out.print("Sorry, you do not have permissions for this page!");
            request.getRequestDispatcher("login.html").include(request, response);
        }

        out.close();
    }
}
