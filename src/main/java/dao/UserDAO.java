package dao;

import entities.User;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import java.util.List;

public class UserDAO {
    private SessionFactory sessionFactory;

    public UserDAO(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public User getByUsername(String name) {
        User u = null;
        Transaction trns;
        Session sess = sessionFactory.openSession();
        try {
            trns = sess.beginTransaction();
            List result = sess.createQuery("from User where username=:username")
                    .setParameter("username", name)
                    .list();
            if(result.size() == 1) {
                u = (User) result.get(0);
            }
            trns.commit();

        } catch (RuntimeException e) {
            e.printStackTrace();
        } finally {
            sess.flush();
            sess.close();
        }
        return u;
    }

    public User signUp(String name,String password) {
        User u = new User(name, password);
        Transaction trns;
        Session sess = sessionFactory.openSession();
        try {
            trns = sess.beginTransaction();
            Integer id = (Integer) sess.save(u);
            trns.commit();


        } catch (RuntimeException e) {
            e.printStackTrace();
        } finally {
            sess.flush();
            sess.close();
        }
        return u;
    }
}  