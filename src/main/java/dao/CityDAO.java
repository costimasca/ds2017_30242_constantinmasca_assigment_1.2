package dao;

import entities.City;
import entities.User;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import java.util.List;

/**
 * Created by constantin on 10/26/17.
 */
public class CityDAO {
    private SessionFactory sessionFactory;

    public CityDAO(SessionFactory sessionFactory) {this.sessionFactory = sessionFactory;}

    public City getById(int id) {
        City c = null;
        Transaction trns;
        Session sess = sessionFactory.openSession();
        try {
            trns = sess.beginTransaction();
            List result = sess.createQuery("from City where id=:id")
                    .setParameter("id", id)
                    .list();
            if(result.size() == 1) {
                c = (City) result.get(0);
            }
            trns.commit();

        } catch (RuntimeException e) {
            e.printStackTrace();
        } finally {
            sess.flush();
            sess.close();
        }
        return c;
    }

    public int getIdByName(String name) {
        City c = null;
        Transaction trns;
        Session sess = sessionFactory.openSession();
        try {
            trns = sess.beginTransaction();
            List result = sess.createQuery("from City where name=:name")
                    .setParameter("name", name)
                    .list();
            if(result.size() == 1) {
                c = (City) result.get(0);
            }
            trns.commit();

        } catch (RuntimeException e) {
            e.printStackTrace();
        } finally {
            sess.flush();
            sess.close();
        }
        return c.getId();
    }

}
