package dao;

import entities.Flight;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

/**
 * Created by constantin on 10/26/17.
 */
public class FlightDAO {
    private SessionFactory sessionFactory;

    public FlightDAO(SessionFactory sessionFactory) { this.sessionFactory = sessionFactory;}

    public List getFlights() {
        List result = null;
        Transaction trns;
        Session sess = sessionFactory.openSession();
        try {
            trns = sess.beginTransaction();
            result = sess.createQuery("from Flight")
                    .list();
            trns.commit();

        } catch (RuntimeException e) {
            e.printStackTrace();
        } finally {
            sess.flush();
            sess.close();
        }
        return result;
    }

    public void saveFlight(Integer number, String plane_type, int depart_city, Timestamp depart_time, int arriv_city, Timestamp arriv_time){
        Transaction trns;
        Session sess = sessionFactory.openSession();
        Flight f = null;
        try {
            trns = sess.beginTransaction();
            f = new Flight(number,plane_type,depart_city,depart_time,arriv_city,arriv_time);
            sess.save(f);
            trns.commit();

        } catch (RuntimeException e) {
            e.printStackTrace();
        } finally {
            sess.flush();
            sess.close();
        }
    }

    public void delete(int id) {
        Transaction trns;
        Session sess = sessionFactory.openSession();
        Flight f;
        try {
            trns = sess.beginTransaction();
            f = sess.load(Flight.class,id);
            if(f!=null) {
                sess.delete(f);
                sess.flush();
            }
            trns.commit();

        } catch (RuntimeException e) {
            e.printStackTrace();
        } finally {
            sess.flush();
            sess.close();
        }
    }

    public Flight getById(int id) {
        List result = getFlights();
        for(int i = 0; i < result.size();i++){
            if(((Flight)result.get(i)).getId() == id){
                return (Flight)result.get(i);
            }
        }
        return null;
    }

    public void update(Integer id, Integer number, String plane_type, int depart_city, Timestamp depart_time, int arriv_city, Timestamp arriv_time) {
        Transaction trns;
        Session sess = sessionFactory.openSession();
        Flight f = null;
        try {
            trns = sess.beginTransaction();
            f = new Flight(id,number,plane_type,depart_city,depart_time,arriv_city,arriv_time);
            sess.update(f);
            trns.commit();

        } catch (RuntimeException e) {
            e.printStackTrace();
        } finally {
            sess.flush();
            sess.close();
        }
    }
}
